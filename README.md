# Xen

The plan is to develop this repo into a compherensive Ansible role for
configuring Xen on Debian.

## xen-shell2

See [xen-shell2](https://github.com/nielsd/xen-shell2) for the fork of [Steve
Kemp's xen-shell](https://xen-tools.org/software/xen-shell/) that this role
uses.

This role is designed to be used with the [users
role](https://git.coop/webarch/users) and [ssh
role](https://git.coop/webarch/ssh) and host variables like this:

```yml
---
users:
  vmuser:
    users_state: present
    users_name: Virtual Server User
    users_ssh_public_keys:
      - https://git.coop/chris.keys
    users_skel: /usr/local/etc/skel.d/xen
    users_groups:
      - xenshell
    users_home_mode: "0700"
    users_shell: /usr/bin/xen-login-shell
...
